package ls.co.econet.ussd;

import com.fasterxml.jackson.databind.ObjectMapper;
import ls.co.econet.ussd.menu.Menu;
import ls.co.econet.ussd.menu.support.MenuRepository;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;

public class MenuRepositoryTest extends UsddMenuLoaderApplicationTests {
    Logger logger = LoggerFactory.getLogger(MenuRepositoryTest.class);
    @Autowired MenuRepository menuRepository;
    @Autowired ResourceLoader resourceLoader;
    @Autowired ObjectMapper objectMapper;

    @Test @Ignore
    public void testFindByServiceKey() throws Exception {
        Menu menu =  menuRepository.findByServiceKey("*196#");
        logger.debug("{}", objectMapper.writeValueAsString(menu));
    }

    @Test
    public void testLoadMenus() throws Exception {
        Resource resource = resourceLoader.getResource("data-prod.json");
        Menu menu = objectMapper.readValue(resource.getFile(), Menu.class);
        menuRepository.save(menu);
    }
}
