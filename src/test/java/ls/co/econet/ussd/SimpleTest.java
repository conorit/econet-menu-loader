package ls.co.econet.ussd;

import org.junit.Test;

import java.text.DecimalFormat;

/**
 * Created by gillin on 2016/01/27.
 */
public class SimpleTest {

    @Test
    public void test() {

        DecimalFormat currencyFormater = new DecimalFormat("M###,###.00");
        float value = 150/100;
        System.out.println(currencyFormater.format(value));
    }
}
