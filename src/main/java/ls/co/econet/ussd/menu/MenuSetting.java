package ls.co.econet.ussd.menu;


import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Entity
@Table(name = "menu_setting")
@Getter @Setter
public class MenuSetting implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @JsonIgnore
    private Long id;

    @Version
    @Column(name = "version")
    @JsonIgnore
    private Integer version;

    @NotNull
    @Column(name="setting_name")
    private String name;

    @NotNull
    @Column(name="setting_value")
    private String value;

    @ManyToOne(fetch= FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name="menu_id")
    @JsonBackReference("menu")
    private Menu menu;

    @Size(max=3)
    private String locale;
}
