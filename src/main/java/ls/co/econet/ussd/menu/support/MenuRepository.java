package ls.co.econet.ussd.menu.support;


import ls.co.econet.ussd.menu.Menu;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MenuRepository extends JpaRepository<Menu, Long> {
    public Menu findByServiceKey(String serviceKey);
}
