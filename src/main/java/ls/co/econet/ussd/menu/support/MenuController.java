package ls.co.econet.ussd.menu.support;

import ls.co.econet.ussd.menu.Menu;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/menus")
public class MenuController {

    final MenuRepository menuRepository;

    @Autowired
    public MenuController(MenuRepository menuRepository){
        this.menuRepository = menuRepository;
    }

    @RequestMapping("/search")
    public Menu findByServiceKey(@RequestParam(value="serviceKey", defaultValue="*196#") String serviceKey){
       return menuRepository.findByServiceKey(serviceKey);
    }
}
