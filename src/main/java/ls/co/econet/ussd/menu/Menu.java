package ls.co.econet.ussd.menu;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

@Entity
@Table(name = "ussd_menu", uniqueConstraints =  @UniqueConstraint(columnNames = "serviceKey"))
@Getter @Setter
public class Menu implements Serializable{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    @JsonIgnore
    private Long id;

    @Version
    @Column(name = "version")
    @JsonIgnore
    private Integer version;

    @Size(max = 128)

    private String serviceKey;

    @NotNull
    @Size(max = 256)
    private String caption;

    @Size(max = 256)
    private String heading;

    @Size(max = 256)
    private String allowedBrandIndexes = "ALL";

    @NotNull
    @Size(max = 3)
    private String paymentType;

    @Size(max = 256)
    private String messageProcessor;

    @NotNull
    @Enumerated(EnumType.STRING)
    MessageProcessorType messageProcessorType;

    @ManyToOne(fetch=FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_id")
    @JsonBackReference("parent")
    private Menu parent;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL)
    @JoinColumn(name = "parent_id")
    @JsonManagedReference("parent")
    private List<Menu> submenus;

    @OneToMany(mappedBy = "menu", fetch= FetchType.EAGER, cascade = CascadeType.ALL)
    @MapKey(name = "name")
    @JsonManagedReference("menu")
    private Map<String, MenuSetting> menuSettings;

    private Boolean active;

    private Boolean weekend;

    @PrePersist
    public void prePersist() {
       for(MenuSetting menuSetting: menuSettings.values()){
           menuSetting.setMenu(this);
       }
    }

    @PreUpdate
    public void preUpdate() {
        for(MenuSetting menuSetting: menuSettings.values()){
            menuSetting.setMenu(this);
        }
    }
}
